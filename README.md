# LICENSE #
All code in this repository doesn't have a license. If you end up using any or all of the code presented here, I just would like you giving me credit. I won't chase you down though, so you're free to do whatever you want.

# WARRANTY #
Likewise, this doesn't have any warranty. It may work, it may not. It may turn your computer into dust or it may not. I won't be held accountable if you decide to put your $200 keyboard on a microwave, making both things explode while you run this application. You've been warned.

# HELP #

## How do I use this application?
If you follow these steps, you should be using the application within 2 minutes. Everything is laid out so you won't have any trouble, given that you follow them correctly.

> Download the source and compile the application

> Go to the [Google Developers Console](https://console.developers.google.com/)

> > Create a new project

> > Give it any name and ID you like

> > Refresh the page and select your project

> > On the left, click **APIs** under **APIs & auth**

> > Turn on **YouTube Data API v3**

> > On the left, click **Credentials** under **APIs & auth**

> > Click on **Create new Key** and then select **Browser key**

> > Don't configure any referer, just press **Create**

> > Jot down/copy your **API key**, *WITHOUT ANY SPACES AROUND IT*

> Go to your [YouTube Account Advanced Settings](https://www.youtube.com/account_advanced)

> > Jot down/copy your **YouTube Channel ID**, *WITHOUT ANY SPACES AROUND IT*

> Go to your [YouTube Account Privacy Settings](https://www.youtube.com/account_privacy)

> > Remove the tick from **Keep all my subscriptions private**

> > Click on **Save**

> Open the application

> > On the top menu, select **Edit** and then **Preferences**

> > Fill the **API Key** and **Channel ID** previously jotted down/copied

> > Press **Save**

> You're all set! Now press **Update** to see it work

## I followed everything correctly, I even double-checked, but I still can't make it work!
You should contact me. I'm still trying to figure out the best way for you to do so. But in the meantime, you can always send me a message on BitBucket.