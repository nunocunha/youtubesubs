﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YouTubeSubs.Controllers;

namespace YouTubeSubs
{
    public partial class PreferencesUI : Form
    {
        private PreferencesController PreferencesController;

        public PreferencesUI()
        {
            PreferencesController = new PreferencesController();
            InitializeComponent();
            PreferencesController.Load(this);
        }

        private void SaveChangesButton_Click(object sender, EventArgs e)
        {
            PreferencesController.Save(this);
            Close();
        }

        private void CancelChangesButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AppendPerLinkCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            AppendPerLinkTextBox.Enabled = AppendPerLinkCheckBox.Checked;
        }

        private void BrowserCommandOpenFileDialogButton_Click(object sender, EventArgs e)
        {
            BrowserCommandOpenFileDialog.ShowDialog();
        }

        private void BrowserCommandOpenFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            var FileDialog = (FileDialog)sender;
            BrowserCommandTextBox.Text = FileDialog.FileName;
        }
    }
}
