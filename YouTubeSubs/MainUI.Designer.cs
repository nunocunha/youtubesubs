﻿namespace YouTubeSubs
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            this.UpdateButton = new System.Windows.Forms.Button();
            this.VideosListView = new System.Windows.Forms.ListView();
            this.VideoDateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VideoTitleColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VideoIdColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StatusLabel = new System.Windows.Forms.Label();
            this.VideosContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.VideosOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.VideosRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VideosCopyLinksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.VideosSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.FileExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TopMenuStrip = new System.Windows.Forms.MenuStrip();
            this.TrayNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TrayUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.TrayExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VideosContextMenuStrip.SuspendLayout();
            this.TopMenuStrip.SuspendLayout();
            this.TrayContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // UpdateButton
            // 
            this.UpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateButton.Location = new System.Drawing.Point(297, 426);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(75, 23);
            this.UpdateButton.TabIndex = 0;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // VideosListView
            // 
            this.VideosListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VideosListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.VideoDateColumnHeader,
            this.VideoTitleColumnHeader,
            this.VideoIdColumnHeader});
            this.VideosListView.FullRowSelect = true;
            this.VideosListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.VideosListView.Location = new System.Drawing.Point(12, 27);
            this.VideosListView.Name = "VideosListView";
            this.VideosListView.Size = new System.Drawing.Size(360, 393);
            this.VideosListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.VideosListView.TabIndex = 1;
            this.VideosListView.UseCompatibleStateImageBehavior = false;
            this.VideosListView.View = System.Windows.Forms.View.Details;
            this.VideosListView.ItemActivate += new System.EventHandler(this.VideosListView_ItemActivate);
            this.VideosListView.SelectedIndexChanged += new System.EventHandler(this.VideosListView_SelectedIndexChanged);
            this.VideosListView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VideosListView_MouseDown);
            // 
            // VideoDateColumnHeader
            // 
            this.VideoDateColumnHeader.Text = "Date published";
            this.VideoDateColumnHeader.Width = 0;
            // 
            // VideoTitleColumnHeader
            // 
            this.VideoTitleColumnHeader.Text = "Video title";
            // 
            // VideoIdColumnHeader
            // 
            this.VideoIdColumnHeader.Text = "Video ID";
            this.VideoIdColumnHeader.Width = 0;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(12, 431);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(24, 13);
            this.StatusLabel.TabIndex = 2;
            this.StatusLabel.Text = "Idle";
            // 
            // VideosContextMenuStrip
            // 
            this.VideosContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.VideosOpenToolStripMenuItem,
            this.toolStripSeparator1,
            this.VideosRemoveToolStripMenuItem,
            this.VideosCopyLinksToolStripMenuItem,
            this.toolStripMenuItem1,
            this.VideosSelectAllToolStripMenuItem});
            this.VideosContextMenuStrip.Name = "VideosContextMenuStrip";
            this.VideosContextMenuStrip.ShowImageMargin = false;
            this.VideosContextMenuStrip.Size = new System.Drawing.Size(105, 104);
            // 
            // VideosOpenToolStripMenuItem
            // 
            this.VideosOpenToolStripMenuItem.Name = "VideosOpenToolStripMenuItem";
            this.VideosOpenToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.VideosOpenToolStripMenuItem.Text = "Open";
            this.VideosOpenToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(101, 6);
            // 
            // VideosRemoveToolStripMenuItem
            // 
            this.VideosRemoveToolStripMenuItem.Name = "VideosRemoveToolStripMenuItem";
            this.VideosRemoveToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.VideosRemoveToolStripMenuItem.Text = "Remove";
            this.VideosRemoveToolStripMenuItem.Click += new System.EventHandler(this.RemoveToolStripMenuItem_Click);
            // 
            // VideosCopyLinksToolStripMenuItem
            // 
            this.VideosCopyLinksToolStripMenuItem.Name = "VideosCopyLinksToolStripMenuItem";
            this.VideosCopyLinksToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.VideosCopyLinksToolStripMenuItem.Text = "Copy links";
            this.VideosCopyLinksToolStripMenuItem.Click += new System.EventHandler(this.CopyLinksToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(101, 6);
            // 
            // VideosSelectAllToolStripMenuItem
            // 
            this.VideosSelectAllToolStripMenuItem.Name = "VideosSelectAllToolStripMenuItem";
            this.VideosSelectAllToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.VideosSelectAllToolStripMenuItem.Text = "Select all";
            this.VideosSelectAllToolStripMenuItem.Click += new System.EventHandler(this.SelectAllToolStripMenuItem_Click);
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileUpdateToolStripMenuItem,
            this.toolStripMenuItem2,
            this.FileExitToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.FileToolStripMenuItem.Text = "File";
            // 
            // FileUpdateToolStripMenuItem
            // 
            this.FileUpdateToolStripMenuItem.Name = "FileUpdateToolStripMenuItem";
            this.FileUpdateToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.FileUpdateToolStripMenuItem.Text = "Update";
            this.FileUpdateToolStripMenuItem.Click += new System.EventHandler(this.UpdateToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(109, 6);
            // 
            // FileExitToolStripMenuItem
            // 
            this.FileExitToolStripMenuItem.Name = "FileExitToolStripMenuItem";
            this.FileExitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.FileExitToolStripMenuItem.Text = "Exit";
            this.FileExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // EditToolStripMenuItem
            // 
            this.EditToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EditPreferencesToolStripMenuItem});
            this.EditToolStripMenuItem.Name = "EditToolStripMenuItem";
            this.EditToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.EditToolStripMenuItem.Text = "Edit";
            // 
            // EditPreferencesToolStripMenuItem
            // 
            this.EditPreferencesToolStripMenuItem.Name = "EditPreferencesToolStripMenuItem";
            this.EditPreferencesToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.EditPreferencesToolStripMenuItem.Text = "Preferences";
            this.EditPreferencesToolStripMenuItem.Click += new System.EventHandler(this.EditPreferencesToolStripMenuItem_Click);
            // 
            // TopMenuStrip
            // 
            this.TopMenuStrip.GripMargin = new System.Windows.Forms.Padding(2);
            this.TopMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.EditToolStripMenuItem});
            this.TopMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.TopMenuStrip.Name = "TopMenuStrip";
            this.TopMenuStrip.Size = new System.Drawing.Size(384, 24);
            this.TopMenuStrip.TabIndex = 4;
            // 
            // TrayNotifyIcon
            // 
            this.TrayNotifyIcon.ContextMenuStrip = this.TrayContextMenuStrip;
            this.TrayNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayNotifyIcon.Icon")));
            this.TrayNotifyIcon.Text = "YouTube Subscription Manager";
            this.TrayNotifyIcon.Visible = true;
            this.TrayNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayNotifyIcon_MouseDoubleClick);
            // 
            // TrayContextMenuStrip
            // 
            this.TrayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TrayUpdateToolStripMenuItem,
            this.toolStripMenuItem3,
            this.TrayExitToolStripMenuItem});
            this.TrayContextMenuStrip.Name = "TrayContextMenuStrip";
            this.TrayContextMenuStrip.ShowImageMargin = false;
            this.TrayContextMenuStrip.Size = new System.Drawing.Size(88, 54);
            // 
            // TrayUpdateToolStripMenuItem
            // 
            this.TrayUpdateToolStripMenuItem.Name = "TrayUpdateToolStripMenuItem";
            this.TrayUpdateToolStripMenuItem.Size = new System.Drawing.Size(87, 22);
            this.TrayUpdateToolStripMenuItem.Text = "Update";
            this.TrayUpdateToolStripMenuItem.Click += new System.EventHandler(this.TrayUpdateToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(84, 6);
            // 
            // TrayExitToolStripMenuItem
            // 
            this.TrayExitToolStripMenuItem.Name = "TrayExitToolStripMenuItem";
            this.TrayExitToolStripMenuItem.Size = new System.Drawing.Size(87, 22);
            this.TrayExitToolStripMenuItem.Text = "Exit";
            this.TrayExitToolStripMenuItem.Click += new System.EventHandler(this.TrayExitToolStripMenuItem_Click);
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.TopMenuStrip);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.VideosListView);
            this.Controls.Add(this.UpdateButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "MainUI";
            this.Text = "YouTube Subscription Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainUI_FormClosing);
            this.Resize += new System.EventHandler(this.MainUI_Resize);
            this.VideosContextMenuStrip.ResumeLayout(false);
            this.TopMenuStrip.ResumeLayout(false);
            this.TopMenuStrip.PerformLayout();
            this.TrayContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.ListView VideosListView;
        private System.Windows.Forms.ColumnHeader VideoDateColumnHeader;
        private System.Windows.Forms.ColumnHeader VideoTitleColumnHeader;
        private System.Windows.Forms.ColumnHeader VideoIdColumnHeader;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.ContextMenuStrip VideosContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem VideosOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem VideosRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem VideosCopyLinksToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem VideosSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FileExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditToolStripMenuItem;
        private System.Windows.Forms.MenuStrip TopMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem EditPreferencesToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon TrayNotifyIcon;
        private System.Windows.Forms.ContextMenuStrip TrayContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem TrayUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem TrayExitToolStripMenuItem;

    }
}

