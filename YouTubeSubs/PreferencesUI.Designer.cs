﻿namespace YouTubeSubs
{
    partial class PreferencesUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreferencesUI));
            this.RemoveAfterOpenCheckBox = new System.Windows.Forms.CheckBox();
            this.CancelChangesButton = new System.Windows.Forms.Button();
            this.SaveChangesButton = new System.Windows.Forms.Button();
            this.ApiKeyLabel = new System.Windows.Forms.Label();
            this.ChannelIdLabel = new System.Windows.Forms.Label();
            this.ApiKeyTextBox = new System.Windows.Forms.TextBox();
            this.ChannelIdTextBox = new System.Windows.Forms.TextBox();
            this.BrowserCommandLabel = new System.Windows.Forms.Label();
            this.BrowserCommandTextBox = new System.Windows.Forms.TextBox();
            this.BrowserCommandOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.BrowserCommandOpenFileDialogButton = new System.Windows.Forms.Button();
            this.AppendPerLinkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.AppendPerLinkCheckBox = new System.Windows.Forms.CheckBox();
            this.AppendPerLinkTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // RemoveAfterOpenCheckBox
            // 
            this.RemoveAfterOpenCheckBox.AutoSize = true;
            this.RemoveAfterOpenCheckBox.Location = new System.Drawing.Point(12, 120);
            this.RemoveAfterOpenCheckBox.Name = "RemoveAfterOpenCheckBox";
            this.RemoveAfterOpenCheckBox.Size = new System.Drawing.Size(165, 17);
            this.RemoveAfterOpenCheckBox.TabIndex = 0;
            this.RemoveAfterOpenCheckBox.Text = "Remove video when opening";
            this.RemoveAfterOpenCheckBox.UseVisualStyleBackColor = true;
            // 
            // CancelChangesButton
            // 
            this.CancelChangesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelChangesButton.Location = new System.Drawing.Point(368, 145);
            this.CancelChangesButton.Name = "CancelChangesButton";
            this.CancelChangesButton.Size = new System.Drawing.Size(75, 23);
            this.CancelChangesButton.TabIndex = 1;
            this.CancelChangesButton.Text = "Cancel";
            this.CancelChangesButton.UseVisualStyleBackColor = true;
            this.CancelChangesButton.Click += new System.EventHandler(this.CancelChangesButton_Click);
            // 
            // SaveChangesButton
            // 
            this.SaveChangesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveChangesButton.Location = new System.Drawing.Point(287, 145);
            this.SaveChangesButton.Name = "SaveChangesButton";
            this.SaveChangesButton.Size = new System.Drawing.Size(75, 23);
            this.SaveChangesButton.TabIndex = 2;
            this.SaveChangesButton.Text = "Save";
            this.SaveChangesButton.UseVisualStyleBackColor = true;
            this.SaveChangesButton.Click += new System.EventHandler(this.SaveChangesButton_Click);
            // 
            // ApiKeyLabel
            // 
            this.ApiKeyLabel.AutoSize = true;
            this.ApiKeyLabel.Location = new System.Drawing.Point(12, 15);
            this.ApiKeyLabel.Name = "ApiKeyLabel";
            this.ApiKeyLabel.Size = new System.Drawing.Size(45, 13);
            this.ApiKeyLabel.TabIndex = 3;
            this.ApiKeyLabel.Text = "API Key";
            // 
            // ChannelIdLabel
            // 
            this.ChannelIdLabel.AutoSize = true;
            this.ChannelIdLabel.Location = new System.Drawing.Point(12, 42);
            this.ChannelIdLabel.Name = "ChannelIdLabel";
            this.ChannelIdLabel.Size = new System.Drawing.Size(60, 13);
            this.ChannelIdLabel.TabIndex = 4;
            this.ChannelIdLabel.Text = "Channel ID";
            // 
            // ApiKeyTextBox
            // 
            this.ApiKeyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ApiKeyTextBox.Location = new System.Drawing.Point(63, 12);
            this.ApiKeyTextBox.Name = "ApiKeyTextBox";
            this.ApiKeyTextBox.Size = new System.Drawing.Size(380, 20);
            this.ApiKeyTextBox.TabIndex = 5;
            // 
            // ChannelIdTextBox
            // 
            this.ChannelIdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChannelIdTextBox.Location = new System.Drawing.Point(78, 39);
            this.ChannelIdTextBox.Name = "ChannelIdTextBox";
            this.ChannelIdTextBox.Size = new System.Drawing.Size(365, 20);
            this.ChannelIdTextBox.TabIndex = 6;
            // 
            // BrowserCommandLabel
            // 
            this.BrowserCommandLabel.AutoSize = true;
            this.BrowserCommandLabel.Location = new System.Drawing.Point(12, 68);
            this.BrowserCommandLabel.Name = "BrowserCommandLabel";
            this.BrowserCommandLabel.Size = new System.Drawing.Size(94, 13);
            this.BrowserCommandLabel.TabIndex = 7;
            this.BrowserCommandLabel.Text = "Browser command";
            // 
            // BrowserCommandTextBox
            // 
            this.BrowserCommandTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BrowserCommandTextBox.Location = new System.Drawing.Point(112, 65);
            this.BrowserCommandTextBox.Name = "BrowserCommandTextBox";
            this.BrowserCommandTextBox.Size = new System.Drawing.Size(301, 20);
            this.BrowserCommandTextBox.TabIndex = 9;
            // 
            // BrowserCommandOpenFileDialog
            // 
            this.BrowserCommandOpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.BrowserCommandOpenFileDialog_FileOk);
            // 
            // BrowserCommandOpenFileDialogButton
            // 
            this.BrowserCommandOpenFileDialogButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BrowserCommandOpenFileDialogButton.Location = new System.Drawing.Point(419, 65);
            this.BrowserCommandOpenFileDialogButton.Name = "BrowserCommandOpenFileDialogButton";
            this.BrowserCommandOpenFileDialogButton.Size = new System.Drawing.Size(24, 20);
            this.BrowserCommandOpenFileDialogButton.TabIndex = 12;
            this.BrowserCommandOpenFileDialogButton.Text = "...";
            this.BrowserCommandOpenFileDialogButton.UseVisualStyleBackColor = true;
            this.BrowserCommandOpenFileDialogButton.Click += new System.EventHandler(this.BrowserCommandOpenFileDialogButton_Click);
            // 
            // AppendPerLinkCheckBox
            // 
            this.AppendPerLinkCheckBox.AutoSize = true;
            this.AppendPerLinkCheckBox.Location = new System.Drawing.Point(12, 94);
            this.AppendPerLinkCheckBox.Name = "AppendPerLinkCheckBox";
            this.AppendPerLinkCheckBox.Size = new System.Drawing.Size(110, 17);
            this.AppendPerLinkCheckBox.TabIndex = 10;
            this.AppendPerLinkCheckBox.Text = "Append per video";
            this.AppendPerLinkToolTip.SetToolTip(this.AppendPerLinkCheckBox, resources.GetString("AppendPerLinkCheckBox.ToolTip"));
            this.AppendPerLinkCheckBox.UseVisualStyleBackColor = true;
            this.AppendPerLinkCheckBox.CheckedChanged += new System.EventHandler(this.AppendPerLinkCheckBox_CheckedChanged);
            // 
            // AppendPerLinkTextBox
            // 
            this.AppendPerLinkTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AppendPerLinkTextBox.Enabled = false;
            this.AppendPerLinkTextBox.Location = new System.Drawing.Point(128, 92);
            this.AppendPerLinkTextBox.Name = "AppendPerLinkTextBox";
            this.AppendPerLinkTextBox.Size = new System.Drawing.Size(315, 20);
            this.AppendPerLinkTextBox.TabIndex = 11;
            this.AppendPerLinkToolTip.SetToolTip(this.AppendPerLinkTextBox, resources.GetString("AppendPerLinkTextBox.ToolTip"));
            // 
            // PreferencesUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 180);
            this.Controls.Add(this.BrowserCommandOpenFileDialogButton);
            this.Controls.Add(this.AppendPerLinkTextBox);
            this.Controls.Add(this.AppendPerLinkCheckBox);
            this.Controls.Add(this.BrowserCommandTextBox);
            this.Controls.Add(this.BrowserCommandLabel);
            this.Controls.Add(this.ChannelIdTextBox);
            this.Controls.Add(this.ApiKeyTextBox);
            this.Controls.Add(this.ChannelIdLabel);
            this.Controls.Add(this.ApiKeyLabel);
            this.Controls.Add(this.SaveChangesButton);
            this.Controls.Add(this.CancelChangesButton);
            this.Controls.Add(this.RemoveAfterOpenCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PreferencesUI";
            this.ShowInTaskbar = false;
            this.Text = "Preferences";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelChangesButton;
        private System.Windows.Forms.Button SaveChangesButton;
        private System.Windows.Forms.Label ApiKeyLabel;
        private System.Windows.Forms.Label ChannelIdLabel;
        private System.Windows.Forms.Label BrowserCommandLabel;
        private System.Windows.Forms.OpenFileDialog BrowserCommandOpenFileDialog;
        private System.Windows.Forms.Button BrowserCommandOpenFileDialogButton;
        private System.Windows.Forms.ToolTip AppendPerLinkToolTip;
        public System.Windows.Forms.TextBox ApiKeyTextBox;
        public System.Windows.Forms.TextBox ChannelIdTextBox;
        public System.Windows.Forms.TextBox BrowserCommandTextBox;
        public System.Windows.Forms.CheckBox AppendPerLinkCheckBox;
        public System.Windows.Forms.TextBox AppendPerLinkTextBox;
        public System.Windows.Forms.CheckBox RemoveAfterOpenCheckBox;
    }
}