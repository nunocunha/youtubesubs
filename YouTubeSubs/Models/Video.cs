﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeSubs.Model
{
    class Video
    {
        public DateTime? Date { get; set; }
        public string Title { get; set; }
        public string Id { get; set; }
    }
}
