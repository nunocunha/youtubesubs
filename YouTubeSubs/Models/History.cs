﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO = System.IO;

namespace YouTubeSubs.Model
{
    class History
    {
        private string FilePath;
        private List<string> FileEntries;
        private List<string> NewEntries;

        public History(string NewFilePath = @"history.dat")
        {
            FilePath = NewFilePath;
            NewEntries = new List<string>();
            Load();
        }

        public void Load()
        {
            CreateIfNotExists();
            FileEntries = IO.File.ReadAllLines(FilePath).ToList();
        }

        public void Add(List<string> Entries)
        {
            foreach (var Entry in Entries)
            {
                Add(Entry);
            }
        }

        public void Add(string Entry)
        {
            if (!Contains(Entry))
            {
                NewEntries.Add(Entry);
            }
        }

        public void Save()
        {
            CreateIfNotExists();
            IO.File.AppendAllLines(FilePath, NewEntries);
            FileEntries.AddRange(NewEntries);
            NewEntries.Clear();
        }

        public bool Contains(string Entry)
        {
            return FileEntries.Contains(Entry) || NewEntries.Contains(Entry);
        }

        private void CreateIfNotExists()
        {
            if (!IO.File.Exists(FilePath))
            {
                IO.File.Create(FilePath).Dispose();
            }
        }
    }
}
