﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using IO = System.IO;

namespace YouTubeSubs.Model
{
    class Preferences
    {
        private static Preferences instance;
        private string FilePath;

        public Options Options { get; set; }

        private Preferences(string NewFilePath = @"config.txt")
        {
            FilePath = NewFilePath;
        }

        public static Preferences Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Preferences();
                    instance.Load();
                }
                return instance;
            }
        }

        public void Load()
        {
            CreateIfNotExists();
            string Json = IO.File.ReadAllText(FilePath);

            try
            {
                Options = JsonConvert.DeserializeObject<Options>(Json);
            }
            catch (Exception)
            {
                Options = new Options();
            }
        }

        public void Save()
        {
            string Json = JsonConvert.SerializeObject(Options, Formatting.Indented);
            IO.File.WriteAllText(FilePath, Json);
        }

        private void CreateIfNotExists()
        {
            if (!IO.File.Exists(FilePath))
            {
                string Json = JsonConvert.SerializeObject(new Options(), Formatting.Indented);
                IO.File.WriteAllText(FilePath, Json);
            }
        }
    }

    internal class Options
    {
        public string ApiKey { get; set; }
        public string ChannelId { get; set; }
        public string BrowserCommand { get; set; }
        public bool AppendPerLink { get; set; }
        public string AppendPerLinkText { get; set; }
        public bool RemoveAfterOpen { get; set; }

        public Options()
        {
            ApiKey = "";
            ChannelId = "";
            BrowserCommand = "";
            AppendPerLink = false;
            AppendPerLinkText = "";
            RemoveAfterOpen = false;
        }
    }
}
