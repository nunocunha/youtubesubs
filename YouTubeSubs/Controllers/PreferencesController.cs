﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeSubs.Model;

namespace YouTubeSubs.Controllers
{
    class PreferencesController
    {
        private Preferences Preferences;

        public PreferencesController()
        {
            Preferences = Preferences.Instance;
        }

        public void Load(PreferencesUI PreferencesUI)
        {
            Preferences.Load();
            PreferencesUI.ApiKeyTextBox.Text = Preferences.Options.ApiKey;
            PreferencesUI.ChannelIdTextBox.Text = Preferences.Options.ChannelId;
            PreferencesUI.BrowserCommandTextBox.Text = Preferences.Options.BrowserCommand;
            PreferencesUI.AppendPerLinkCheckBox.Checked = Preferences.Options.AppendPerLink;
            PreferencesUI.AppendPerLinkTextBox.Text = Preferences.Options.AppendPerLinkText;
            PreferencesUI.RemoveAfterOpenCheckBox.Checked = Preferences.Options.RemoveAfterOpen;
        }

        public void Save(PreferencesUI PreferencesUI)
        {
            Preferences.Options.ApiKey = PreferencesUI.ApiKeyTextBox.Text;
            Preferences.Options.ChannelId = PreferencesUI.ChannelIdTextBox.Text;
            Preferences.Options.BrowserCommand = PreferencesUI.BrowserCommandTextBox.Text;
            Preferences.Options.AppendPerLink = PreferencesUI.AppendPerLinkCheckBox.Checked;
            Preferences.Options.AppendPerLinkText = PreferencesUI.AppendPerLinkTextBox.Text;
            Preferences.Options.RemoveAfterOpen = PreferencesUI.RemoveAfterOpenCheckBox.Checked;
            Preferences.Save();
        }
    }
}
