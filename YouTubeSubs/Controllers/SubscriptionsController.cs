﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Services;
using YouTubeSubs.Model;
using YouTubeApi = Google.Apis.YouTube.v3;

namespace YouTubeSubs.Controllers
{
    class SubscriptionsController
    {
        private const int MaxRetries = 7;
        private History History;
        private Preferences Preferences;
        private SemaphoreSlim NetworkOperation;

        public SubscriptionsController()
        {
            History = new History();
            Preferences = Preferences.Instance;
            NetworkOperation = new SemaphoreSlim(50);
        }

        public bool RemoveAfterOpen()
        {
            return Preferences.Options.RemoveAfterOpen;
        }

        public bool AppendPerLink()
        {
            return Preferences.Options.AppendPerLink;
        }

        public string AppendPerLinkText()
        {
            return Preferences.Options.AppendPerLinkText;
        }

        public string BrowserCommand()
        {
            return Preferences.Options.BrowserCommand;
        }

        public void AddToHistory(List<string> VideoIds)
        {
            History.Add(VideoIds);
            History.Save();
        }

        public void SaveHistory()
        {
            History.Save();
        }

        public async Task<List<string[]>> GetVideoItemList()
        {
            List<string[]> Items = new List<string[]>();

            if (Preferences.Options.ApiKey != "" && Preferences.Options.ChannelId != "")
            {
                List<string> Subscriptions = await GetSubscriptions();
                List<string> UploadPlaylists = await GetUploadPlaylists(Subscriptions);
                List<string> PlaylistItems = await GetPlaylistsItems(UploadPlaylists);

                List<Video> Videos = await GetVideos(PlaylistItems);

                foreach (var Video in Videos)
                {
                    Items.Add(new string[] { Video.Date.Value.ToString("yyyyMMddHHmmssfff"), Video.Title, Video.Id });
                }
            }

            return Items;
        }

        private async Task<List<string>> GetSubscriptions()
        {
            List<string> Channels = new List<string>();

            Tuple<string, List<string>> Page = await GetSubscriptionPage();
            Channels.AddRange(Page.Item2);

            while (Page.Item1 != "")
            {
                Page = await GetSubscriptionPage(Page.Item1);
                Channels.AddRange(Page.Item2);
            }

            return Channels;
        }

        private async Task<Tuple<string, List<string>>> GetSubscriptionPage(string PageToken = "")
        {
            var SubscriptionListRequest = NewYouTubeService().Subscriptions.List("snippet");
            SubscriptionListRequest.ChannelId = Preferences.Options.ChannelId;
            SubscriptionListRequest.MaxResults = 50;
            SubscriptionListRequest.PageToken = PageToken;

            await NetworkOperation.WaitAsync();
            var SubscriptionListResponse = await SubscriptionListRequest.ExecuteAsync();
            NetworkOperation.Release();

            List<string> Channels = new List<string>();

            foreach (var SubscriptionResult in SubscriptionListResponse.Items)
            {
                if (SubscriptionResult.Kind == "youtube#subscription")
                {
                    Channels.Add(SubscriptionResult.Snippet.ResourceId.ChannelId);
                }
            }

            string NextPageToken = SubscriptionListResponse.NextPageToken;
            NextPageToken = NextPageToken == null ? "" : NextPageToken;

            return new Tuple<string, List<string>>(NextPageToken, Channels);
        }

        private async Task<List<string>> GetUploadPlaylists(List<string> Channels)
        {
            int ChunkSize = 50;
            List<string> ChannelChunks = JoinInChunks(Channels, ",", ChunkSize);
            List<Task<List<string>>> GetUploadPlaylistsTasks = new List<Task<List<string>>>();
            List<string> UploadPlaylists = new List<string>();

            foreach (var Chunk in ChannelChunks)
            {
                GetUploadPlaylistsTasks.Add(GetUploadPlaylistsFromChunk(Chunk, ChunkSize));
            }

            while (GetUploadPlaylistsTasks.Count > 0)
            {
                Task<List<string>> FinishedTask = await Task.WhenAny(GetUploadPlaylistsTasks);
                GetUploadPlaylistsTasks.Remove(FinishedTask);
                List<string> ChunkPlaylists = await FinishedTask;
                UploadPlaylists.AddRange(ChunkPlaylists);
            }

            return UploadPlaylists;
        }

        private async Task<List<string>> GetUploadPlaylistsFromChunk(string ChannelChunk, int ChunkSize)
        {
            var ChannelListRequest = NewYouTubeService().Channels.List("contentDetails");
            ChannelListRequest.Id = ChannelChunk;
            ChannelListRequest.MaxResults = ChunkSize;

            await NetworkOperation.WaitAsync();
            var ChannelListResponse = await ChannelListRequest.ExecuteAsync();
            NetworkOperation.Release();

            List<string> Playlists = new List<string>();

            foreach (var ChannelResult in ChannelListResponse.Items)
            {
                Playlists.Add(ChannelResult.ContentDetails.RelatedPlaylists.Uploads);
            }

            return Playlists;
        }

        private async Task<List<string>> GetPlaylistsItems(List<string> Playlists)
        {
            List<Task<List<string>>> GetPlaylistsItemsTasks = new List<Task<List<string>>>();
            List<string> PlaylistsItems = new List<string>();

            foreach (var Playlist in Playlists)
            {
                GetPlaylistsItemsTasks.Add(GetPlaylistItems(Playlist));
            }

            while (GetPlaylistsItemsTasks.Count > 0)
            {
                Task<List<string>> FinishedTask = await Task.WhenAny(GetPlaylistsItemsTasks);
                GetPlaylistsItemsTasks.Remove(FinishedTask);
                List<string> PlaylistItems = await FinishedTask;
                PlaylistsItems.AddRange(PlaylistItems);
            }

            return PlaylistsItems;
        }

        private async Task<List<string>> GetPlaylistItems(string Playlist)
        {
            var PlaylistItemListRequest = NewYouTubeService().PlaylistItems.List("contentDetails");
            PlaylistItemListRequest.PlaylistId = Playlist;
            PlaylistItemListRequest.MaxResults = 10;

            await NetworkOperation.WaitAsync();
            var PlaylistItemListResponse = await PlaylistItemListRequest.ExecuteAsync();
            NetworkOperation.Release();

            List<string> Items = new List<string>();

            foreach (var PlaylistItemResult in PlaylistItemListResponse.Items)
            {
                if (PlaylistItemResult.Kind == "youtube#playlistItem")
                {
                    if (!History.Contains(PlaylistItemResult.ContentDetails.VideoId))
                    {
                        Items.Add(PlaylistItemResult.ContentDetails.VideoId);
                    }
                }
            }

            return Items;
        }

        private async Task<List<Video>> GetVideos(List<string> Items)
        {
            int ChunkSize = 50;
            List<string> VideoChunks = JoinInChunks(Items, ",", ChunkSize);
            List<Task<List<Video>>> GetVideosTasks = new List<Task<List<Video>>>();
            List<Video> Videos = new List<Video>();

            foreach (var Chunk in VideoChunks)
            {
                GetVideosTasks.Add(GetVideosFromChunk(Chunk, ChunkSize));
            }

            while (GetVideosTasks.Count > 0)
            {
                Task<List<Video>> FinishedTask = await Task.WhenAny(GetVideosTasks);
                GetVideosTasks.Remove(FinishedTask);
                List<Video> ChunkVideos = await FinishedTask;
                Videos.AddRange(ChunkVideos);
            }

            return Videos;
        }

        private async Task<List<Video>> GetVideosFromChunk(string VideoChunk, int ChunkSize)
        {
            var VideoListRequest = NewYouTubeService().Videos.List("snippet");
            VideoListRequest.Id = VideoChunk;

            await NetworkOperation.WaitAsync();
            var VideoListResponse = await VideoListRequest.ExecuteAsync();
            NetworkOperation.Release();

            List<Video> Videos = new List<Video>();

            foreach (var VideoResult in VideoListResponse.Items)
            {
                Video Video = new Video()
                {
                    Date = VideoResult.Snippet.PublishedAt,
                    Title = VideoResult.Snippet.Title,
                    Id = VideoResult.Id
                };

                if (Video.Date.Value >= DateTime.Now.AddDays(-30))
                {
                    Videos.Add(Video);
                }
            }

            return Videos;
        }

        private static List<string> JoinInChunks(List<string> InputList, string Delimiter, int ChunkSize)
        {
            List<string> Chunks = new List<string>();
            string NewChunk = "";
            int Count = 0;

            foreach (var Element in InputList)
            {
                NewChunk += Element + Delimiter;
                Count++;

                if (Count % ChunkSize == 0 || Count == InputList.Count)
                {
                    Chunks.Add(NewChunk.Substring(0, NewChunk.Length - Delimiter.Length));
                    NewChunk = "";
                }
            }

            return Chunks;
        }

        private YouTubeApi.YouTubeService NewYouTubeService()
        {
            return new YouTubeApi.YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = Preferences.Options.ApiKey,
                ApplicationName = "YouTubeSubs"
            });
        }
    }
}
