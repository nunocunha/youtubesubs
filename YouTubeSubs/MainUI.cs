﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using YouTubeSubs.Controllers;

namespace YouTubeSubs
{
    public partial class MainUI : Form
    {
        private SubscriptionsController SubscriptionsController;
        private bool Hidden;

        private static string LinkFormat = "https://youtu.be/{0}?nc";

        public MainUI()
        {
            SubscriptionsController = new SubscriptionsController();
            Hidden = false;
            InitializeComponent();
            UiResize();
            UpdateVideos();
            UpdateVideosContextMenu();
        }

        private void MainUI_Resize(object sender, EventArgs e)
        {
            UiResize();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            UpdateVideos();
        }

        private void VideosListView_ItemActivate(object sender, EventArgs e)
        {
            OpenSelectedVideos();
        }

        private void VideosListView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                VideosContextMenuStrip.Show(Cursor.Position);
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenSelectedVideos();
        }

        private void UpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateVideos();
        }

        private void VideosListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateVideosContextMenu();
        }

        private void RemoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveSelectedVideos();
        }

        private void SelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectAllVideos();
        }

        private void CopyLinksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyLinksToClipboard();
        }

        private void UiResize()
        {
            VideoTitleColumnHeader.Width = VideosListView.Size.Width - SystemInformation.VerticalScrollBarWidth - 20;
        }

        private void UpdateVideosContextMenu()
        {
            bool HasItems = VideosListView.Items.Count > 0;
            bool HasSelectedItems = VideosListView.SelectedItems.Count > 0;

            VideosOpenToolStripMenuItem.Enabled = HasSelectedItems;
            VideosRemoveToolStripMenuItem.Enabled = HasSelectedItems;
            VideosCopyLinksToolStripMenuItem.Enabled = HasSelectedItems;

            VideosSelectAllToolStripMenuItem.Enabled = HasItems;
        }

        private async void UpdateVideos()
        {
            VideosListView.Enabled = false;
            UpdateButton.Enabled = false;
            FileUpdateToolStripMenuItem.Enabled = false;
            TrayUpdateToolStripMenuItem.Enabled = false;
            StatusLabel.Text = "Updating";

            List<string[]> Videos = await SubscriptionsController.GetVideoItemList();

            VideosListView.Items.Clear();
            foreach (var Video in Videos)
            {
                VideosListView.Items.Add(new ListViewItem(new string[] { Video[0], Video[1], Video[2] }));
            }

            UpdateVideosContextMenu();

            VideosListView.Enabled = true;
            UpdateButton.Enabled = true;
            FileUpdateToolStripMenuItem.Enabled = true;
            TrayUpdateToolStripMenuItem.Enabled = true;
            StatusLabel.Text = "Idle";
        }

        private void OpenSelectedVideos()
        {
            bool ErrorOccured = false;
            string MultipleLinksArguments = "";
            bool JoinMultipleLinks = SubscriptionsController.AppendPerLink();

            foreach (ListViewItem Item in VideosListView.SelectedItems)
            {
                string VideoId = Item.SubItems[2].Text;

                if (JoinMultipleLinks)
                {
                    MultipleLinksArguments += String.Format(" {0} {1}", SubscriptionsController.AppendPerLinkText(), String.Format(LinkFormat, VideoId));
                }
                else
                {
                    ProcessStartInfo ProcessInfo = new ProcessStartInfo(SubscriptionsController.BrowserCommand(), String.Format(LinkFormat, VideoId));

                    try
                    {
                        Process.Start(ProcessInfo);
                    }
                    catch (Exception)
                    {
                        ErrorOccured = true;
                    }
                }
            }

            if (JoinMultipleLinks)
            {
                ProcessStartInfo ProcessInfo = new ProcessStartInfo(SubscriptionsController.BrowserCommand(), MultipleLinksArguments);

                try
                {
                    Process.Start(ProcessInfo);
                }
                catch (Exception)
                {
                    ErrorOccured = true;
                }
            }

            if (!ErrorOccured && SubscriptionsController.RemoveAfterOpen())
            {
                RemoveSelectedVideos();
            }
        }

        private void RemoveSelectedVideos()
        {
            List<string> VideosToRemove = new List<string>();

            foreach (ListViewItem Item in VideosListView.SelectedItems)
            {
                string VideoId = Item.SubItems[2].Text;
                Item.Remove();
                VideosToRemove.Add(VideoId);
            }

            SubscriptionsController.AddToHistory(VideosToRemove);
        }

        private void SelectAllVideos()
        {
            foreach (ListViewItem Item in VideosListView.Items)
            {
                Item.Selected = true;
            }
        }

        private void CopyLinksToClipboard()
        {
            List<string> Links = new List<string>();

            foreach (ListViewItem Item in VideosListView.SelectedItems)
            {
                string VideoId = Item.SubItems[2].Text;
                Links.Add(String.Format(LinkFormat, VideoId));
            }

            Clipboard.SetText(String.Join(Environment.NewLine, Links));
        }

        private void EditPreferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenPreferencesWindow();
        }

        private void OpenPreferencesWindow()
        {
            PreferencesUI PreferencesForm = new PreferencesUI();
            PreferencesForm.Show(this);
        }

        private void CloseWindow()
        {
            SubscriptionsController.SaveHistory();
            Application.Exit();
        }

        private void HideWindow()
        {
            foreach (var Child in OwnedForms)
            {
                Child.Hide();
            }

            Hide();

            Hidden = true;
        }

        private void ShowWindow()
        {
            WindowState = FormWindowState.Minimized;
            Show();
            WindowState = FormWindowState.Normal;

            foreach (var Child in OwnedForms)
            {
                Child.WindowState = FormWindowState.Minimized;
                Child.Show();
                Child.WindowState = FormWindowState.Normal;
            }

            Hidden = false;
        }

        private void TrayNotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (Hidden)
            {
                ShowWindow();
            }
            else
            {
                HideWindow();
            }
        }

        private void MainUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                HideWindow();
                e.Cancel = true;
            }
        }

        private void TrayUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateVideos();
        }

        private void TrayExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseWindow();
        }
    }
}
